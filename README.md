# gitlab labeler

These scripts fill a gitlab project or group with labels.

I use these scripts for new repositories or groups in order to have a fixed set of labels supporting my workflow.

*Note:* priority is ignored for group labels, it can only be applied for project labels (see [gitlab docs](https://docs.gitlab.com/ee/user/project/labels.html#set-label-priority)).

See [changelog](changelog.md) for all changes.


**Table of contents**

[[_TOC_]]


## Example

See <https://gitlab.com/groups/etg-gitlab/-/labels> for the set of labels defined in this repository in action.
For these I used the python version and the call:

~~~shell
$ ./add-labels.sh -g 11937126
~~~

and the configuration file

~~~properties
[private]
url = https://gitlab.com/
private_token = .............
~~~

The full definition of a label is as follows:

~~~json
{
	"name": "Priority: Critical",
	"color": "#B03517",
	"text_color": "#FFFFFF",
	"priority": 0
}
~~~

Mandatory values are: `name` and `color`.
Optional value `text_color` is set to `#FFFFFF` per default.
Empty priorities are not set for the label.


## Scripts

There are two types of script:

1. python
2. curl with fmpp

I am using *docker* calls in order to be able to use the scripts without much installation hassle.


### python

This script is written in *python* (*docker* call) and uses the *python-gitlab* library.
One could surely use the *python-gitlab* CLI but I like the script version better.

1. define your labels in `defaults/labels.json`
2. define your private configuration (including API token) in `defaults/private_config.cfg`, you can use template for that

	**Important:** don't commit your private token in the config file to a public repository.
3. call `add-labels.sh` in order to add the labels to a project or a group, both identified by their ID

- [python-gitlab](https://github.com/python-gitlab/python-gitlab)


### curl with fmpp

These scripts are my first attempt.
They call the API using *curl* and process the label files with *fmpp* (*docker* call).

1. define your labels in `label-definitions/labels.json`
2. call `process-json.sh` in order to create single label files
3. call `add-labels.sh` in order to add the labels to a project
4. if you want your labels to be group labels, call `promote-labels.sh`

These scripts work but are a bit clumsy, yet - better than nothing...

- [curl](https://curl.se)
- [fmpp](https://fmpp.sourceforge.net/)



## Copyright, license

(c) copyright 2021-2023, Ekkart Kleinod, ekleinod@edgesoft.de

gitlab labeler is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
