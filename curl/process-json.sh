#!/bin/bash

# This file is part of gitlab labeler.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

CONTAINER_FMPP=fmpp
IMAGE_FMPP=ekleinod/$CONTAINER_FMPP
WORKDIR_FMPP=/local

# help
function showHelp() {
  echo "NAME"
  echo -e "\tprocess-json - process json file with fmpp"
  echo
  echo "SYNOPSIS"
  echo -e "\tprocess-json -f JSON-FILE -o OUTPUT-DIR -t TEMPLATE-DIR [OPTIONS]"
  echo
  echo "DESCRIPTION"
  echo -e "\tThis script processes a single json file with fmpp using the given templates."
  echo
  echo -e "\t-f\tjson file"
  echo -e "\t-o\toutput directory"
  echo -e "\t-t\ttemplate directory"
  echo -e "\t-h\tshow this help"
  echo
}

# parameters
while getopts hf:o:t: option
do
	case "${option}"
		in
      f)
        JSONFILE=${OPTARG}
        ;;
      o)
        OUTPUTDIR=${OPTARG}
        ;;
      t)
        TEMPLATEDIR=${OPTARG}
        ;;
      h | *)
        showHelp
        exit 0
        ;;
	esac
done

# mandatory arguments
if [ -z "$JSONFILE" ]
then
  echo "mandatory option -f is missing"
  showHelp
  exit 0
fi

if [ -z "$OUTPUTDIR" ]
then
  echo "mandatory option -o is missing"
  showHelp
  exit 0
fi

if [ -z "$TEMPLATEDIR" ]
then
  echo "mandatory option -t is missing"
  showHelp
  exit 0
fi

mkdir --parents $OUTPUTDIR
rm $OUTPUTDIR/*
docker run \
		--rm \
		--name $CONTAINER_FMPP \
		--volume "${PWD}:$WORKDIR_FMPP" \
		$IMAGE_FMPP \
		--data="labels:json($WORKDIR_FMPP/$JSONFILE)" --source-root=$WORKDIR_FMPP/$TEMPLATEDIR --source-encoding=UTF-8 --output-root=$WORKDIR_FMPP/$OUTPUTDIR --output-encoding=UTF-8

# EOF
