#!/bin/bash

# This file is part of gitlab labeler.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

CONTAINER_FMPP=fmpp
IMAGE_FMPP=ekleinod/$CONTAINER_FMPP
WORKDIR_FMPP=/local

CURLOUT=curl-out
CURLFILE=labels.json
FMPPOUT=fmpp-out


# help
function showHelp() {
  echo "NAME"
  echo -e "\tpromote-labels - promote all labels to group labels"
  echo
  echo "SYNOPSIS"
  echo -e "\tpromote-labels -r REPO-URL -p PROJECT-ID -d TEMP-DIR [OPTIONS]"
  echo
  echo "DESCRIPTION"
  echo -e "\tThis script promotes all labels of a project to group labels."
  echo
  echo -e "\t-d\ttemporary directory"
  echo -e "\t-p\tproject id"
  echo -e "\t-r\trepository url"
  echo -e "\t-t\taccess token for repository"
  echo -e "\t-h\tshow this help"
  echo
}

# parameters
while getopts hd:p:r:t: option
do
	case "${option}"
		in
      d)
        TEMPDIR=${OPTARG}
        ;;
      p)
        PROJECTID=${OPTARG}
        ;;
      r)
        REPOURL=${OPTARG}
        ;;
      t)
        TOKEN=${OPTARG}
        ;;
      h | *)
        showHelp
        exit 0
        ;;
	esac
done

# mandatory arguments
if [ -z "$TEMPDIR" ]
then
  echo "mandatory option -d is missing"
  showHelp
  exit 0
fi

if [ -z "$REPOURL" ]
then
  echo "mandatory option -r is missing"
  showHelp
  exit 0
fi

if [ -z "$PROJECTID" ]
then
  echo "mandatory option -p is missing"
  showHelp
  exit 0
fi

# execution
function promoteLabel() {

  curl \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $TOKEN" \
  --request PUT \
  "$REPOURL/api/v4/projects/$PROJECTID/labels/$1/promote"

}

rm -r $TEMPDIR
mkdir $TEMPDIR
mkdir $TEMPDIR/$CURLOUT
mkdir $TEMPDIR/$FMPPOUT

curl \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $TOKEN" \
  --request GET \
  --output $TEMPDIR/$CURLOUT/$CURLFILE \
  "$REPOURL/api/v4/projects/$PROJECTID/labels/"

./process-json.sh -f $TEMPDIR/$CURLOUT/$CURLFILE -o $TEMPDIR/$FMPPOUT -t fmpp-templates-promote

for file in $TEMPDIR/$FMPPOUT/*
do
  labelid=`expr "$file" : "$TEMPDIR/$FMPPOUT/\(.*\).json"`
  promoteLabel $labelid
done

rm -r $TEMPDIR

# EOF
