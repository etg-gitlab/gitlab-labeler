#!/bin/bash

# This file is part of gitlab labeler.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

# help
function showHelp() {
  echo "NAME"
  echo -e "\tadd-labels - adds labels to gitlab repository"
  echo
  echo "SYNOPSIS"
  echo -e "\tadd-labels [-f LABEL-FILE | -d LABEL-DIR] -r REPO-URL -p PROJECT-ID [OPTIONS]"
  echo
  echo "DESCRIPTION"
  echo -e "\tThis script adds all labels defined in a json file to the given repository."
  echo
  echo -e "\t-d\tdirectory with json files with labels"
  echo -e "\t-f\tjson file with single label"
  echo -e "\t-p\tproject id"
  echo -e "\t-r\trepository url"
  echo -e "\t-t\taccess token for repository"
  echo -e "\t-h\tshow this help"
  echo
}

# parameters
while getopts hd:f:p:r:t: option
do
	case "${option}"
		in
      d)
        LABELDIR=${OPTARG}
        ;;
      f)
        LABELFILE=${OPTARG}
        ;;
      p)
        PROJECTID=${OPTARG}
        ;;
      r)
        REPOURL=${OPTARG}
        ;;
      t)
        TOKEN=${OPTARG}
        ;;
      h | *)
        showHelp
        exit 0
        ;;
	esac
done

# mandatory arguments
if [ -z "$LABELDIR" ] && [ -z "$LABELFILE" ]
then
  echo "either option -d or option -f is missing"
  showHelp
  exit 0
fi

if [ -n "$LABELDIR" ] && [ -n "$LABELFILE" ]
then
  echo "both options -d and -f were declared"
  showHelp
  exit 0
fi

if [ -z "$REPOURL" ]
then
  echo "mandatory option -r is missing"
  showHelp
  exit 0
fi

if [ -z "$PROJECTID" ]
then
  echo "mandatory option -p is missing"
  showHelp
  exit 0
fi

# execution
function createLabel() {

  curl \
  --verbose \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $TOKEN" \
  --request POST \
  --data "@$1" \
  "$REPOURL/api/v4/projects/$PROJECTID/labels"

}

if [ -n "$LABELFILE" ]
then
  createLabel $LABELFILE
fi

if [ -n "$LABELDIR" ]
then
  for file in $LABELDIR/*
  do
    createLabel $file
  done
fi

# EOF
