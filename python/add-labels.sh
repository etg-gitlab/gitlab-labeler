#!/bin/bash

# This file is part of gitlab labeler.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)


echo "Add labels to gitlab project or group"
echo

# help
function showHelp() {
  echo "NAME"
  echo -e "\tadd-labels - adds labels to gitlab project or group"
  echo
  echo "SYNOPSIS"
  echo -e "\tadd-labels -g GITLAB-ID [-l LABEL-FILE] [-c CONFIG-DIR]"
  echo
  echo "DESCRIPTION"
  echo -e "\tThis script adds all labels defined in a json file to the given repository or group."
  echo
  echo -e "\t-g\tgitlab id of project or group"
  echo -e "\t-l\tjson file with label definition (default: defaults/labels.json)"
  echo -e "\t-c\tconfig directory (default: defaults)"
  echo -e "\t-h\tshow this help"
  echo
}

# parameters
LABELFILE=defaults/labels.json
CONFIGDIR=defaults
while getopts hg:l:c: option
do
	case "${option}"
		in
      g)
        GITLABID=${OPTARG}
        ;;
      l)
        LABELFILE=${OPTARG}
        ;;
      c)
        CONFIGDIR=${OPTARG}
        ;;
      h | *)
        showHelp
        exit 0
        ;;
	esac
done

# mandatory arguments
if [ -z "$GITLABID" ]
then
  echo "mandatory option -g (gitlab id) is missing"
  echo
  showHelp
  exit 0
fi

CONTAINER_PYTHON=python
IMAGE_PYTHON=ekleinod/$CONTAINER_PYTHON
WORKDIR_PYTHON=/usr/src/myapp

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINER_PYTHON \
	--volume "${PWD}":$WORKDIR_PYTHON \
	--workdir $WORKDIR_PYTHON \
	$IMAGE_PYTHON \
		python-gitlab/add-labels.py \
			--gitlabid $GITLABID \
			--labels $LABELFILE \
			--config $CONFIGDIR
