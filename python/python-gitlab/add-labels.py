# This file is part of gitlab labeler.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

import argparse
import gitlab
import json
import logging
import os
import sys

from source_helper import read_json_file, save_json_file

# authenticate at gitlab
def authenticate(config_dir):

	logging.debug("Authenticating...")

	config_files = []
	for filename in filter(lambda filename: (filename.endswith(".cfg") and ("template" not in filename)), os.listdir(os.path.join(os.getcwd(), config_dir))):
		config_files.append(os.path.join(os.getcwd(), config_dir, filename))

	logging.debug("Config files '{}'.".format(config_files))

	gitlab_api = gitlab.Gitlab.from_config(config_files=config_files)
	gitlab_api.auth()

	return gitlab_api


# read labels
def read_labels(labelfile):

	logging.debug("Read labels from {}...".format(labelfile))

	labels = read_json_file(labelfile)

	logging.debug("Read {} labels from {}...".format(len(labels), labelfile))

	return labels


# get project or group with id
def get_label_parent(gitlab_api, gitlab_id):

	logging.debug("Find project {}.".format(gitlab_id))
	try:
		labelparent = gitlab_api.projects.get(gitlab_id)
	except Exception as e:
		logging.debug("Find group {}.".format(gitlab_id))
		try:
			labelparent = gitlab_api.groups.get(gitlab_id)
		except Exception as e:
			raise Exception("Neither project nor group found for id {}".format(gitlab_id))

	logging.debug("Found '{}'.".format(labelparent.name))

	return labelparent


# add project or group labels
def add_labels(gitlab_api, gitlab_id, labels):

	labelparent = get_label_parent(gitlab_api, gitlab_id)

	for label in labels:
		logging.info("Adding label '{}' to '{}'.".format(label.get("name"), labelparent.name))

		labeldef = {
			"name": label.get("name"),
			"color": label.get("color"),
			"text_color": label.get("text_color", "#FFFFFF")
			}

		if ("priority" in label):
			labeldef["priority"] = label.get("priority")

		labelparent.labels.create(labeldef)

	return


# init args parser, read and save sources
if __name__ == "__main__":

	logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s', datefmt='%Y-%m-%d | %H:%M:%S', level=logging.INFO)

	parser = argparse.ArgumentParser(description = "Add labels to gitlab project or group.")
	parser.add_argument("-c", "--config", type = str, help = "config directory with config files for private token, url, etc. ", required = True)
	parser.add_argument("-g", "--gitlabid", type = str, help = "gitlab id of project or group", required = True)
	parser.add_argument("-l", "--labels", type = str, help = "label file", required = True)
	parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

	args = parser.parse_args()

	try:

		if (args.verbose):
			logging.getLogger().setLevel(logging.DEBUG)


		gitlab_api = authenticate(args.config)
		labels = read_labels(args.labels)
		add_labels(gitlab_api, args.gitlabid, labels)


	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		logging.error("%s in %s, line %d | %s", type(e).__name__, fname, exc_tb.tb_lineno, e)
